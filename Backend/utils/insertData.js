const db = require("../app/models");
const personas = [
  {
    tipoDoc: "DNI",
    numeroDoc: "357654159",
    apellidos: "Yalico Meza",
    nombre: "Jhonatan",
    fechaNac: "2001-02-25",
    numCelular: "654852951",
    numTelf: "064 152425",
    correo: "yalico@gmail.com",
    direccion: "Jr. San Carlos 365",
    estado: "CON SÍNTOMAS"
  },
];

async function insertData() {
  console.log("Iniciando la insercion de tablas");
  console.log("-----------------------------");
  // CategoriaProducto
  for (const persona of personas) {
    await db.Persona.create(persona);
  }
  console.log("-----------------------------");
  console.log("Insercion de tablas finalizado");
}

// ejecucion de la funcion
insertData();
