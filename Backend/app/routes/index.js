var express = require('express');
var router = express.Router();

const personaCtrl = require('../controllers/persona.controller');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
});

router.get('/personas/', personaCtrl.list);
router.get('/personas/:id', personaCtrl.get);
router.post('/personas/', personaCtrl.create);
router.put('/personas/:id', personaCtrl.update);
router.delete('/personas/:id', personaCtrl.remove);

module.exports = router;
