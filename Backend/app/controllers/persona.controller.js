const db = require('../models');
const Persona = db.Persona;

//GET
//Listar todos
exports.list = async function(req, res) {
  try {
    const personas = await Persona.findAll({
    });
    res.status(200).json(personas);
  }catch(err) {
    res.status(500).json({error: err});
  }
}
//Listar por id
exports.get = async function(req, res) {
  try {
    const id = req.params.id;
    const persona = await Persona.findOne(
      {
        where: {id: id},
      }
    );
    res.status(200).json(persona);
  } catch(err) {
    res.status(500).json({error: err});
  }
}

//POST
//crear Persona
exports.create = async function(req, res) {
  try{
      const nuevaPersona = await Persona.create(req.body);
      res.status(201).json(nuevaPersona);
  } catch(err){
      res.status(500).json({error: err});
  }
};

//PUT
//actualizar Persona
exports.update = async function(req, res) {
  const id = req.params.id;
  try {
      await Persona.update(req.body,{where: {id: id}});
      res.status(201).json({ok: 'ok'})
  } catch(error){}
};

//DELETE
//eliminar Persona
exports.remove = async function(req, res) {
  const id = req.params.id;
  try {
      await Persona.destroy({where: {id: id} });
      res.status(200).json({ok: 'ok'})
        
  } catch(error){
      res.status(500).json({error: err});
  }
};
