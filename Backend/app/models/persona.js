'use strict';
module.exports = (sequelize, DataTypes) => {
   const Persona = sequelize.define('Persona', 
    {
       id: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          autoIncrement: true,
       },
       tipoDoc: DataTypes.STRING(30),
       numeroDoc: DataTypes.STRING(30),
       apellidos: DataTypes.STRING,
       nombre: DataTypes.STRING(70),
       fechaNac: DataTypes.DATEONLY,
       numCelular: DataTypes.STRING(25),
       numTelf: DataTypes.STRING(25),
       correo: DataTypes.STRING,
       direccion: DataTypes.STRING,
       estado: DataTypes.STRING,
    },
   );
     //Traer las categorias
   Persona.associate = (models)=>{
   };
    return Persona;
 }