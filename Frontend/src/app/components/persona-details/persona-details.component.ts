import { Component, OnInit } from '@angular/core';
import { PersonaService } from 'src/app/services/persona.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-persona-details',
  templateUrl: './persona-details.component.html',
  styleUrls: ['./persona-details.component.css']
})
export class PersonaDetailsComponent implements OnInit {
  currentPersona = null;
  message = '';

  constructor(
    private personaService: PersonaService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.message = '';
    this.getPersonas(this.route.snapshot.paramMap.get('id'));
  }

  getPersonas(id) {
    this.personaService.get(id)
      .subscribe(
        data => {
          this.currentPersona = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
  updatePersona() {
    this.personaService.update(this.currentPersona.id, this.currentPersona)
      .subscribe(
        response => {
          console.log(response);
          this.message = 'Los datos de la persona han sido actualizados!';
        },
        error => {
          console.log(error);
        });
  }

  deletePersona() {
    this.personaService.delete(this.currentPersona.id)
      .subscribe(
        response => {
          console.log(response);
          this.router.navigate(['/personas']);
        },
        error => {
          console.log(error);
        });
  }
}
