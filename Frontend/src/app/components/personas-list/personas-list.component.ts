import { Component, OnInit } from '@angular/core';
import { PersonaService } from 'src/app/services/persona.service';

@Component({
  selector: 'app-personas-list',
  templateUrl: './personas-list.component.html',
  styleUrls: ['./personas-list.component.css']
})
export class PersonasListComponent implements OnInit {

  personas: any;
  currentPersona = null;
  currentIndex = -1;
  dni = '';

  constructor(private personaService: PersonaService) { }

  ngOnInit() {
    this.retrievePersonas();
  }
  
  retrievePersonas() {
    this.personaService.getAll()
      .subscribe(
        data => {
          this.personas = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }
  
  refreshList() {
    this.retrievePersonas();
    this.currentPersona = null;
    this.currentIndex = -1;
  }

  setActivePersona(persona, index) {
    this.currentPersona = persona;
    this.currentIndex = index;
  }
}
