import { Component, OnInit } from "@angular/core";
import { PersonaService } from "src/app/services/persona.service";

@Component({
  selector: "app-add-persona",
  templateUrl: "./add-persona.component.html",
  styleUrls: ["./add-persona.component.css"]
})
export class AddPersonaComponent implements OnInit {
  persona = {
    tipoDoc: "",
    numeroDoc: "",
    apellidos: "",
    nombre: "",
    fechaNac: new Date(),
    numCelular: "",
    numTelf: "",
    correo: "",
    direccion: "",
    estado: ""
  };
  submitted = false;

  constructor(private personaService: PersonaService) {}

  ngOnInit() {}

  savePersona() {
    const data = {
      tipoDoc: this.persona.tipoDoc,
      numeroDoc: this.persona.numeroDoc,
      nombre: this.persona.nombre,
      apellidos: this.persona.apellidos,
      fechaNac: this.persona.fechaNac,
      numCelular: this.persona.numCelular,
      numTelf: this.persona.numTelf,
      correo: this.persona.correo,
      direccion: this.persona.direccion,
      estado: this.persona.estado,
    };

    this.personaService.create(data).subscribe(
      response => {
        console.log(response);
      },
      error => {
        console.log(error);
      }
    );

    this.submitted = true;
  }

  newPersona() {
    this.submitted = false;
    this.persona = {
      tipoDoc: "",
      numeroDoc: "",
      apellidos: "",
      nombre: "",
      fechaNac: new Date(),
      numCelular: "",
      numTelf: "",
      correo: "",
      direccion: "",
      estado: ""
    };
  }
}
